import React, { memo, JSX } from 'react';
import Wrapper from './wrapper';

const Loader: JSX = () => (
  <Wrapper>
    <div className="lds-dual-ring">...</div>
  </Wrapper>
);

export default memo(Loader);
