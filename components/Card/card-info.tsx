import React, { FC, memo } from 'react';
import Tags from './tags';
import { ElementProps } from '../interfases'
const baseImgUrl = 'https://picsum.photos';

interface CardProps extends ElementProps {
    body: string,
    tag: string[]
}

const CardInfo: FC<CardProps> = ({ id, title, body, tags }) => (
  <div className="card-info">
    <img src={`${baseImgUrl}/id/${id}/500/500`} alt="" />
    <h2>{title}</h2>
    <p className="body">{body}</p>
    <Tags tags={tags} />
  </div>
);

export default memo(CardInfo);
