import React, { FC, JSX } from 'react';
import Link from 'next/link';
import { useQuery } from '@apollo/client';
import { useRouter } from 'next/router';

import CardInfo from './card-info';
import Wrapper from './wrapper';
import NavWrapper from './nav-wrapper';
import getDetails from '../../queries/get-details';

const Details: FC = () => {
  const router = useRouter();
  const details = getDetails<string>(router.query.id);
  const { loading, error, data } = useQuery(details);

  if (loading) return 'Loading...';
  if (error) return `Error! ${error.message}`;
  const post = data.post;
  const { id, title, body } = post;

  return (
    <>
      <NavWrapper>
        <div className="nav">
          <Link href={'/'}>Back</Link>
        </div>
      </NavWrapper>
      <Wrapper>
        <CardInfo id={id} title={title} body={body} tags={title.split(' ')} />
      </Wrapper>
    </>
  );
};

export default Details;
