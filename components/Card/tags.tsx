import React, { FC } from 'react';

const Tags: FC = ({ tags }) => {
  const renderTags = () => {
    return tags.map(el => <li>{el}</li>);
  };

  return <ul className="tags">{renderTags()}</ul>;
};

export default Tags;
