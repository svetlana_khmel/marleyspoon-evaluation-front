import styled from 'styled-components';

const Wrapper = styled.div`
  .nav a {
    list-style: none;
    display: inline;
    float: left;
    font-size: 18px;
    background-color: darkred;
    color: #fff;
    padding: 15px;
    cursor: pointer;
    top: 64px;
    position: absolute;

    &:hover {
      background: #f5f5f5;
      color: darkred;
    }
  }
`;

export default Wrapper;
