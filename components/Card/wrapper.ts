import styled from 'styled-components';

const Wrapper = styled.div`
  ul.tags {
    padding: 0;
    
        li {
        list-style: none;
        display: inline;
        float: left;
        font-size: 18px;
        background-color: darkred;
        color: #fff;
        padding: 15px;
        cursor: pointer;
        border-radius: 20px;
    }
    
  } 
  
  .card-info {
    font-family: Arial;
    margin-top: 140px;

    h2 {
        font-size: 26px;
    }
    
    p {
        font-size: 24px;
    }
  } 
`;

export default Wrapper;
