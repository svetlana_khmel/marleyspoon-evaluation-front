export interface ElementProps {
    id: number;
    title: string;
    image?: string;
}