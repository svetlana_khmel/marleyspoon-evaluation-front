import React, { FC, useState, SyntheticEvent } from 'react';
import { useQuery } from '@apollo/client';
import Loader from '../Loader';

import ListItem from '../ListItem/item';
import GET_ALL_RECIPES from '../../queries/get-all-recipes';
import { ElementProps } from '../interfases';

const List: FC = () => {
  const { loading, error, data } = useQuery(GET_ALL_RECIPES);
  const [currentPage, setCurrentPage] = useState<number>(1);
  const itemsPerPage = 10;

  const handlePagination = (e: SyntheticEvent<EventTarget>, num: number) => {
    setCurrentPage(num);
  };

  const renderPagination = (data: Array<ElementProps>) => {
    let pageNumbers = [];

    for (let i = 1; i <= data.length / itemsPerPage; i++) {
      pageNumbers = [...pageNumbers, i];
    }

    return pageNumbers.map(i => {
      return (
        <li className={'left'} key={i + 'pag'} onClick={e => handlePagination(e, i)}>
          {i}
        </li>
      );
    });
  };

  if (loading) return <Loader />;
  if (error) return `Error! ${error.message}`;

  const indexOfLastTodo = currentPage * itemsPerPage;
  const indexOfFirstTodo = indexOfLastTodo - itemsPerPage;
  const listdata = data.posts.data;
  let dataTorender = data.posts.data.slice(indexOfFirstTodo, indexOfLastTodo);

  return (
    <div className="List">
      <ul>{dataTorender.map((el, i) => <ListItem key={el.id} id={el.id} title={el.title} />)}</ul>
      <ul className="pagination">{renderPagination(listdata)}</ul>
    </div>
  );
};

export default List;
