import styled from 'styled-components';

const Wrapper = styled.div`
  ul.pagination {
    padding: 0;
    margin-left: 66px;
    
    li {
    list-style: none;
    display: inline;
    float: left;
    font-size: 18px;
    background-color: darkred;
    color: #fff;
    padding: 15px;
    cursor: pointer;

    &:hover {
      background: #f5f5f5;
      color: darkred;
    }
  }
    
  }  
   
`;

export default Wrapper;
