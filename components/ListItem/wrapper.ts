import styled from 'styled-components';

const Wrapper = styled.div`
  a {
    text-decoration: none;
  }  

  li {
    padding: 15px;
    text-decoration: none;
    display: block;
    cursor: pointer;

    &:hover {
      background: #f5f5f5;

      h3 {
        color: #387ef5;
      }
    }
  }

  h3 {
    color: #222;
    font-weight: bold;
    font-size: 1.75rem;
    line-height: 35px;
    font-family: 'PT Sans', sans-serif;
    text-transform: capitalize;
    margin: 0;
  }
  
  img {
    float: left;
    margin: 0 10px 10px;
  }
`;

export default Wrapper;
