import React, { FC } from 'react';
import Link from 'next/link';

import Wrapper from './wrapper';
import { ElementProps } from '../interfaces';
const baseImgUrl = 'https://picsum.photos';

const ListItem: FC<ElementProps> = ({ id, title }) => {
  return (
    <Wrapper>
      <li>
        <Link href={`/details?id=${id}`}>
          <a>
            <img src={`${baseImgUrl}/id/${id}/40/40`} alt="" />
            <h3>{title}</h3>
          </a>
        </Link>
      </li>
    </Wrapper>
  );
};

export default ListItem;
