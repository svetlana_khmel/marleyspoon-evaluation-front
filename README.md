### General

Screenshot:

![Scheme](https://res.cloudinary.com/dmt6v2tzo/image/upload/v1613749571/Screen_Shot_2021-02-19_at_5.45.31_PM_zh4exk.png)
![Scheme](https://res.cloudinary.com/dmt6v2tzo/image/upload/v1613749571/Screen_Shot_2021-02-19_at_5.45.43_PM_n9agnv.png)



* Use React + Next.js to build the different views
* Any code must be in Typescript
* In case you need any API endpoints, create a separated Node.js app
* You must use GraphQL with Apollo Client/Server
* In case you need a database, use Postgres
* Provide very clear and detailed instructions on how to run your solution locally
* No deployment is needed
* IMPORTANT: All solutions must be as production ready as possible. We will mainly take a look at how modular and scalable your app can be, and how you write tests. For the rest, we recommend just to leave a section in the readme with "Future improvements", so that you save some time.
* If you have any implementation detail question, just ask! You can create a GitHub discussion on your repo, add GHTM to it and talk.

## Bonus

Use Styled-components
Provide a Docker compose file to run all apps

  
  
## Part 1
  You need to create a simple acquisition funnel for Marley Spoon!

It will consist of 3 simple steps (take a look to the current Marley Spoon funnel to get an idea; no need to implement the same UI):


## Select plan: 


the user should select a plan (for 2 or 4 people), meals per week for the selected plan and show the price per portion and the total price per week.
Enter your data and delivery preferences: the user should enter their email, password, delivery information (first name, last name, address, city and country) and the delivery day from the available ones.
Summary: we show a summary of the previous steps (meals per week, plan type, delivery day, delivery slot and total amount) and a button to continue.
After the summary step, the following should happen:

- A new user is created in the system
- The new user is authenticated in the app (keep it simple and make some notes on how would you improve what you did)
- The user is redirected to the view "recipes", where they can see the list of the available recipes



## Part 2



You will show a list of Marley Spoon's recipes. You will need to create a sample web application that uses the Contentful API to fetch the data. There should be 2 views (different URL path for each one):

A list view of all the recipes: display a preview of all recipes, including Title and Image.
Recipe details view: display Title, Image, List of Tags, Description, Chef Name.
When clicking on a recipe on the list view, you should then show the details view.

```bash
```

### Credentials
We are providing you with an API key and Space ID to an API for Marley Spoon recipes on Contentful (Content Delivery API). All the information necessary will be available in Contentul's documentation.

- The Space ID is: kk2bw5ojx476
- The Environment ID (if necessary) is: master
- The Access Token is: 7ac531648a1b5e1dab6c18b0979f822a5aad0fe5f1109829b8a197eb2be4b84c

-https://gist.github.com/GHMT/7dc15e87d6971cb6bd1a8dead1f297c8

## Installation

```bash
npm install
```

## Usage

```python
npm run dev
```



### Fake graphql api

https://graphqlzero.almansi.me/

### Fake img API

-https://picsum.photos/id/237/200/300
