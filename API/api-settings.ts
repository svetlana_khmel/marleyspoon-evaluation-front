const baseImgUrl = 'https://picsum.photos/';
const baseAPIURL = 'https://graphqlzero.almansi.me/api';

export {
    baseImgUrl,
    baseAPIURL
}