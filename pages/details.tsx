import React from 'react';
import { ApolloProvider, ApolloClient, InMemoryCache } from '@apollo/client';
import Card from '../components/Card';
import { baseAPIURL } from '../API/api-settings';

const client = new ApolloClient({
  cache: new InMemoryCache(),
  uri: baseAPIURL
});

const Details = () => (
  <ApolloProvider client={client}>
    <Card />
  </ApolloProvider>
);

export default Details;
