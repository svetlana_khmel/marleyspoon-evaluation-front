import React from 'react';
import { ApolloProvider, ApolloClient, InMemoryCache } from '@apollo/client';
import List from '../components/List';
import Wrapper from '../components/List/wrapper';
import { baseAPIURL } from '../API/api-settings';

const client = new ApolloClient({
  cache: new InMemoryCache(),
  uri: baseAPIURL
});

const IndexComponent = () => (
  <ApolloProvider client={client}>
    <Wrapper>
      <List />
    </Wrapper>
  </ApolloProvider>
);

export default IndexComponent;
