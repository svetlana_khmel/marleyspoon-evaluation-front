import { gql } from '@apollo/client';

const getDetails = (query) => {
    const GET_DETAILS = gql`
      query {
          post(id: ${query}) {
            id
            title
            body
  }
}
`;
    return GET_DETAILS;
};

export default getDetails;
