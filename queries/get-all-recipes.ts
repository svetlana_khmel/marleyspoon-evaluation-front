import { gql } from '@apollo/client';

const GET_ALL_RECIPES = gql`
  query($options: PageQueryOptions) {
    posts(options: $options) {
      data {
        id
        title
      }
      meta {
        totalCount
      }
    }
  }
`;

export default GET_ALL_RECIPES;